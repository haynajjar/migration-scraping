# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

# server "example.fr", user: "deploy", roles: %w{app db web}, my_property: :my_value
# server "example.fr", user: "deploy", roles: %w{app web}, other_property: :other_value
# server "db.example.fr", user: "deploy", roles: %w{db}



# role-based syntax
# ==================

# Defines a role with one or multiple servers. The primary server in each
# group is considered to be the first unless any hosts have the primary
# property set. Specify the username and a domain or IP for the server.
# Don't use `:all`, it's a meta role.

# role :app, %w{deploy@example.fr}, my_property: :my_value
# role :web, %w{user1@primary.fr user2@additional.fr}, other_property: :other_value
# role :db,  %w{deploy@example.fr}



# Configuration
# =============
# You can set any configuration variable like in config/deploy.rb
# These variables are then only loaded and set in this stage.
# For available Capistrano configuration variables see the documentation page.
# http://capistranorb.fr/documentation/getting-started/configuration/
# Feel free to add new variables to customise your setup.



# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult the Net::SSH documentation.
# http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# The server-based syntax can be used to override options:
# ------------------------------------
# server "example.fr",
#   user: "user_name",
#   roles: %w{web app},
#   ssh_options: {
#     user: "user_name", # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: "please use keys"
#   }


set :rails_env, 'staging'
set :deploy_via, :remote_cache

# set :branch, `git rev-parse --abbrev-ref test`.chomp
set :branch, 'master'

# set :ssh_options, { :forward_agent => true, :port => 22 }
set :keep_releases, 5
# default_run_options[:pty] = true
# set :default_env, { rbenv_bin_path: '~/.rbenv/bin' }
server "mscraping.fouita.fr", [:app, :web, :db, :primary => true]


server 'mscraping.fouita.fr',
  user: 'azureuser',
  roles: %w{web app},
  ssh_options: {
    user: 'azureuser', # overrides user setting above
    keys: %w(/home/azureuser/.ssh/id_rsa),
    forward_agent: false,
    auth_methods: %w(publickey password)
    # password: 'please use keys'
  }

