class AuthenticationController < ApplicationController


	def sign_in
		result = User.authenticate user_params[:email], user_params[:password]
		if result[:status] == :success
			set_current_user result[:user] 
			redirect_to admin_index_path, notice: "Authenticated Successfully"
		else
			flash[:alert] = "Wrong email or password"
			render :login
		end 
	end

	def set_current_user user
		set_user_session user.id
	end

	private
	def user_params
		params.require(:user).permit(:email, :password)
	end

end