class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :user_authenticated?

  def user_authenticated?
  	if user_session.nil? && controller_name.to_sym != :authentication 
  		redirect_to login_path
  	elsif user_session.present? && controller_name.to_sym == :authentication
  		redirect_to admin_index_path
  	end
  end

  protected
  def set_user_session user_id
  	session[:user_id] = user_id
  end

  private
  def user_session
  	session[:user_id]
  end	


end
