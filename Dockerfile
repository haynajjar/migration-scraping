FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /migration-scraping
WORKDIR /migration-scraping
ADD Gemfile /migration-scraping/Gemfile
ADD Gemfile.lock /migration-scraping/Gemfile.lock
RUN bundle install
ADD . /migration-scraping