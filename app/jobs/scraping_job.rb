class ScrapingJob < ApplicationJob
  
  RUN_EVERY = 1.hour

  def perform scraping_origin
    # do your thing
    scraping_result = ScrapingService.check_import scraping_origin.link
    if scraping_result[:success]
    	data = scraping_result[:scraping_data]
      initial_target_f = ScrapingDatum.convert_to_num(data[:initial_target]) rescue 0
      collected_amount_f = ScrapingDatum.convert_to_num(data[:collected_amount]) rescue 0
    	ScrapingDatum.create(scraping_origin: scraping_origin, collected_amount: data[:collected_amount], collected_amount_f: collected_amount_f, initial_target_f: initial_target_f, initial_target: data[:initial_target], nb_contributors: data[:nb_contributors], end_date: data[:end_date])
    	ScrapingService.update_frequency scraping_origin, scraping_origin.end_date
    end
    if scraping_origin.frequency != 0
    	self.class.set(wait: scraping_origin.frequency.minutes).perform_later scraping_origin
    end
  end

end