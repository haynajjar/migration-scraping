class AddEndDateToScrapingOrigins < ActiveRecord::Migration[5.0]
  def change
    add_column :scraping_origins, :end_date, :date
  end
end
