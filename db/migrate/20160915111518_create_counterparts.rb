class CreateCounterparts < ActiveRecord::Migration[5.0]
  def change
    create_table :counterparts do |t|
      t.integer :amount
      t.string :title
      t.string :description
      t.datetime :shipping_date
      t.string :name
      t.integer :max_number
      t.integer :current_number
      t.boolean :to_deliver
      t.string :counterpart_type
      t.string :notes
      t.integer :campaign_id
      t.string :image
      t.string :type
      t.integer :barred_price
      t.boolean :sisplay_barred_price
      t.integer :deposit
      t.boolean :cash_deposit
      t.boolean :highlight
      t.boolean :display_variant
      t.integer :shipper_rule_id
      

      t.timestamps
    end
  end
end
