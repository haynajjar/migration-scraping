class CreateScrapingData < ActiveRecord::Migration[5.0]
  def change
    create_table :scraping_data do |t|
      t.string :collected_amount
      t.string :initial_target
      t.string :nb_contributors
      t.string :end_date
      t.integer :scraping_origin_id

      t.timestamps
    end
  end
end
