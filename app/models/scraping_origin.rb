class ScrapingOrigin < ApplicationRecord
	belongs_to :scraping_source
	has_many :scraping_data
end
