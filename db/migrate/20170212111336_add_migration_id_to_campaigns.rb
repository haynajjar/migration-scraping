class AddMigrationIdToCampaigns < ActiveRecord::Migration[5.0]
  def change
    add_column :campaigns, :migration_id, :string
  end
end
