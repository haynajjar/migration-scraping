class Digisponsor::CampaignService < Digisponsor::CoreService

	
	class << self
		

		def migrate
			# get list campaigns ordered by created desc
			# we assume that the list is ordered (it seems like that..)
			# CHECK if migration already started
			unless migration_started?
				migration_started
				begin
					init_logger
					logger.info "Call API campaign ..."
					response = call_campaigns
					if response[:success]
						start_migration response[:campaigns]
					else
						# can't find campaigns
						logger.error "Can't call campaign API , #{response[:message]}"
					end
					migration_finished
				rescue StandardError => e 
					migration_finished
					logger.error e.message
					p e.backtrace
				end
			else
				{success: false, message: "Migration already started"}
			end
		end


		def call_campaigns
			if cookies[:expires].present? && cookies[:expires].to_datetime > DateTime.now
				{success: true, campaigns: get_campaigns}
			else
				# LOG inti cookies
				logger.info "Authenticate ...."
				if authenticate
					response = get_campaigns
					# LOG campaigns issue
					{success: true, campaigns: response}
				else
					# LOG authentication problem in campaigns call
					{success: false, message: "can't authenticate"}
				end
			end
		end

		def start_migration campaigns
			# LOG Start campaigns migration ..
			logger.info "Start migration ..."

			campaigns.each do |campaign|
				logger.info "Campaign #{campaign['entry_id']}"
				logger.info "Check existing campaigns ..."
				if Campaign.find_by(migration_id: campaign["entry_id"]).nil?					
					logger.info "#{campaign['entry_id']} is a new campaign"
					migrate_campaign campaign, :create
				else
					logger.info "campaign #{campaign['entry_id']} exists" 
					migrate_campaign campaign, :update 
				end
			end
		end


		def migrate_campaign campaign, action=:create
			campaign_hash = {migration_id: campaign["entry_id"],
							essential_description_image: extract_image(campaign["files"]),
							name: campaign["name"],
							type_campaign: "Collecter des dons",
							short_url: extract_uri_path(campaign["uri_paths"]),
							pitch_video_url: extract_video_url(campaign["links"]),
							headline: campaign["blurb"],
							funding_status: extract_funding_status(campaign),
							categories: extract_categories(campaign["categories"]),
							essential_objective_goal_funding: campaign["funding_goal"],
							essential_objective_currency: extract_currency(campaign["currencies"]),
							campaign_start_date: campaign["starts_date_time"],
							campaign_duration: campaign["campaign_duration"],
							campaign_limit_date: campaign["ends_date_time"],
							campaign_status: campaign["entry_status"]
							}
			if action == :update
				logger.info "Update campaign #{campaign['entry_id']}"
				Campaign.find_by(migration_id: campaign["entry_id"]).update(campaign_hash)
			elsif action == :create 
				logger.info "Create new campaign #{campaign['entry_id']}"
				Campaign.create(campaign_hash)
			end


		end


		def extract_image files
			base_uri_image+files.map{|f| f if f["resource_content_type"] == "image" }.compact.first["path_external"] if files && files.any?
		end


		def extract_uri_path paths 
			paths.first["path"] if paths && paths.any?
		end

		def extract_video_url links
			links.map{|l| l if l["resource_content_type"] == "video"}.compact.first["uri"] if links && links.any?
		end


		def extract_funding_status campaign
			campaign["maximum_allowed_funds_raised"] == 0 ? "Flexible" : "Tout ou rien"
		end

		def extract_categories categories
			(categories && categories.any?) ? categories.map{|cat| cat["name"]} : []
		end


		def extract_currency currencies
			currencies.first["name"] if currencies && currencies.any?
		end



		def get_campaigns
			logger.info "Getting campiagns list ..."
			get('/campaign')
		end

		def logger
			@@campaign_logger 
		end

		def init_logger
			begin
				File.delete("#{Rails.root}/log/digisponsor_campaign_migration.log")
			rescue StandardError => e 
				puts "Logger File Not found, creating new one ..."
			ensure
				@@campaign_logger = Logger.new("#{Rails.root}/log/digisponsor_campaign_migration.log")
			end
		end

	


	end
end