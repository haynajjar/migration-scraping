class Admin::CampaignsController < AdminController

	def index
		@campaigns = Campaign.page(params[:page]).per_page(10)
	end

end