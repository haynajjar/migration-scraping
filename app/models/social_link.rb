class SocialLink < ApplicationRecord
	belongs_to :user, optional: true
end
