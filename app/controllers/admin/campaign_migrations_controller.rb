class Admin::CampaignMigrationsController < AdminController

	def index
		@migration_log = File.read("#{Rails.root}/log/digisponsor_campaign_migration.log").gsub("\n","<br>").html_safe rescue "No log file"
		@migration_started = Digisponsor::CampaignService.migration_started?
	end

	def start
		# start migration job
		if Digisponsor::CampaignService.migration_started?
			render json: {success: false, message: "Migration Already Started"}
		else
			Digisponsor::CampaignService.migration_init
			Digisponsor::MigrateCampaignJob.perform_later
			render json: {success: true, redirect: show_log_admin_campaign_migrations_path}
		end
	end

	def show_log
		# show log every second
		@migration_log = File.read("#{Rails.root}/log/digisponsor_campaign_migration.log").gsub("\n","<br>").html_safe
		render json: {log: @migration_log.gsub("\n","<br>").html_safe, migration_finished: !Digisponsor::CampaignService.migration_started?}
	end

end