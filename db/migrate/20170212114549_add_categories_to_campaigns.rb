class AddCategoriesToCampaigns < ActiveRecord::Migration[5.0]
  def change
    add_column :campaigns, :categories, :string, array: true
  end
end
