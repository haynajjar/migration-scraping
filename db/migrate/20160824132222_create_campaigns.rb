class CreateCampaigns < ActiveRecord::Migration[5.0]
  def change   
    create_table :campaigns do |t|
      t.string  :url
      t.string  :type_campaign
      t.string  :short_url 
      t.string  :name 
      t.string  :headline 
      t.string  :funding_status 
      t.string  :campaign_status 
      t.integer :current_funding 
      t.string  :id_stripe 
      t.string  :stripe_payment_token 
      t.string  :essential_description_title 
      t.string  :essential_description_presentation 
      t.string  :essential_description_image 
      t.string  :essential_description_type 
      t.string  :essential_description_subtype 
      t.string  :essential_objective_type #radio button
      t.integer :essential_objective_goal_funding #objectif (Montant)
      t.integer :essential_objective_minimum_funding  #montant minimal
      t.string  :essential_objective_currency #devise
      
      t.integer :essential_objective_goal_commands 
      t.integer :essential_objective_minimum_commands  #quantite minimum
      t.integer :essential_objective_count_commands #incrementation
       
      t.integer :essential_objective_goal_votes  #objectif (votes)
      t.integer :essential_objective_count_votes 
      t.integer :essential_objective_minimum_votes 
      
      t.string  :pc_f_name 
      t.string  :pc_l_name 
      t.datetime :pc_birth_date
      t.string   :pc_phone 
      t.string  :pc_country 
      t.string  :pc_street
      t.string  :pc_city 
      t.integer :pc_postal_code
      
      t.datetime :campaign_start_date 
      t.integer :campaign_duration 
      t.datetime :campaign_limit_date 
      t.string  :pitch_video_url 
      t.string  :pitch_category 
      t.string  :pitch_description 
      t.string  :pitch_page_adress 
      t.integer :pitch_audience_number 
      t.boolean :pitch_is_event 
      t.boolean :stripe_available
      t.string  :pitch_place_name 
      t.string  :pitch_place_adress 
      t.string  :about 
      t.string  :about_html 
      t.string  :approved_at 
      t.datetime  :created_at
      t.string  :notes 
      t.datetime  :updated_at 
      t.string  :facebook_link 
      t.string  :messenger_link 
      t.string  :twitter_link  
      t.datetime :repayment_deadline
      t.string  :pitch_tags     , array: true
      t.integer :rise_mode
      t.string  :campaign_classification
      t.string :forther_information
      t.string :chaeckout_message
      t.string :twitter_message
      t.string :facebook_message
      t.string :headline_text_above
      t.string :subheadline_text_above
      t.string :text_below_form
      t.string :service_tearm_url
      t.string :privancy_policy_url
      t.datetime :pitch_fin_evenement
      t.datetime :picht_debut_evenemet
   end
  end
end
