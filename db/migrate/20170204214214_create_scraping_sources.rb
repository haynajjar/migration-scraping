class CreateScrapingSources < ActiveRecord::Migration[5.0]
  def change
    create_table :scraping_sources do |t|
      t.string :name
      t.string :url
      t.string :ext_collected_amount
      t.string :ext_initial_target
      t.string :ext_nb_contributors
      t.string :ext_end_date

      t.timestamps
    end
  end
end
