class ScrapingDatum < ApplicationRecord
	belongs_to :scraping_origin

	scope :recent, ->{ order(created_at: :desc) }
	
	def self.convert_to_num str
		str.scan(/[.,0-9]/).join.gsub(/,(\d{2}$)/,'.\1').gsub(",","").to_f
	end
end
