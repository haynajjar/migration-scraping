class User < ApplicationRecord

	has_many :social_links
	has_many :business_organizations


	# This method exist in the existing application
	def self.authenticate email, password
		user = User.find_by(email: email)
		if user.present?
			# This just a way of a simple authentication since we didn't agree on the implementation details (it's already done)
			if user.password_hash == hashed_pass(user.salt.to_s+password)
				return {status: :success, user: user}
			end
		end
		{status: :error}
	end

	def full_name
		[name,l_name].join(" ")
	end

	def address
		[lign1,lign2,cp, ville].join(" ")
	end

	private
	def self.hashed_pass password
		Digest::SHA256.hexdigest password
	end
end