class Digisponsor::CounterpartService < Digisponsor::CoreService

	
	class << self
		

		def migrate
			# get list counterparts ordered by created desc
			# we assume that the list is ordered (it seems like that..)
			# CHECK if migration already started
			unless migration_started?
				migration_started
				begin
					init_logger
					logger.info "Look for local campaign..."
					campaigns = get_local_campaigns 
					if campaigns.any?
						campaigns.each do |campaign|
							response = call_counterparts campaign.migration_id 
							if response[:success]
								start_migration response[:counterparts], campaign

							else
								logger.error "Can't call level-entry API , #{response[:message]}"
							end
						end
						migration_finished
					else
						logger.error "No campaign founded, Please migrate campaigns and then come back"
					end

				rescue StandardError => e 
					migration_finished
					logger.error e.message
					p e.backtrace
				end
			else
				{success: false, message: "Migration already started"}
			end
		end


		def call_counterparts entry_id
			if cookies[:expires].present? && cookies[:expires].to_datetime > DateTime.now
				{success: true, counterparts: get_counterparts(entry_id)}
			else
				# LOG inti cookies
				logger.info "Authenticate ...."
				if authenticate
					response = get_counterparts(entry_id)
					# LOG counterparts issue
					{success: true, counterparts: response}
				else
					# LOG authentication problem in counterparts call
					{success: false, message: "can't authenticate"}
				end
			end
		end

		def start_migration counterparts, campaign
			# LOG Start counterparts migration ..
			logger.info "Start migration ..."

			counterparts.each do |counterpart|
				logger.info "Counterpart #{counterpart['entry_id']}"
				logger.info "Check existing counterparts ..."
				if Counterpart.find_by(migration_id: counterpart["id"]).nil?					
					logger.info "#{counterpart['id']} is a new counterpart"
					migrate_counterpart counterpart, :create, campaign
				else
					logger.info "counterpart #{counterpart['id']} exists" 
					migrate_counterpart counterpart, :update, campaign
				end
			end
		end



		def migrate_counterpart counterpart, action=:create , campaign
			counterpart_hash = {
								amount: counterpart["amount"],
								title: counterpart["name"],
								description: counterpart["description"],
								max_number: counterpart["item_limit"],
								shipping_date: counterpart["estimated_delivery_time"],
								migration_id: counterpart["id"],
								campaign_id: campaign.id
								}

			if action == :update
				logger.info "Update counterpart #{counterpart['id']}"
				Counterpart.find_by(migration_id: counterpart["id"]).update(counterpart_hash)
			elsif action == :create 
				logger.info "Create new counterpart #{counterpart['id']}"
				Counterpart.create(counterpart_hash)
			end

		end

		def get_local_campaigns
			logger.info "Getting local campiagns list ..."
			Campaign.all
		end

		def get_counterparts entry_id
			logger.info "Calling pledge-level (counterparts) for campaign #{entry_id}..."
			get("/campaign/#{entry_id}/pledge-level")
		end

		def logger
			@@counterparts_logger 
		end

		def init_logger
			begin
				File.delete("#{Rails.root}/log/digisponsor_counterparts_migration.log")
			rescue StandardError => e 
				puts "Logger File Not found, creating new one ..."
			ensure
				@@counterparts_logger = Logger.new("#{Rails.root}/log/digisponsor_counterparts_migration.log")
			end
		end

	


	end
end