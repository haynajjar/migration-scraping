document.addEventListener("turbolinks:load", function() {

	if($("#submit_check_import_form").length > 0)
		$("#submit_check_import_form").prop('disabled', true);
	
	$("#certified_source_checkbox").change(function() {
	    if(this.checked) {
			$("#submit_check_import_form").prop('disabled', false);
	    }else{
			$("#submit_check_import_form").prop('disabled', true);
	    }
	});

	$("#submit_scraping_source").off('click').on('click', function(){
		$("#create_scraping_source").submit();

		$("#create_scraping_source").on("ajax:success", function(evt , result, status){
			$(this).find("input[type=text], textarea").val("");
			if (result.success){
				if(!result.exist){
					source = result.scraping_source
					$("#scraping_sources_table tbody").append('<tr><td>'+source.name+'</td><td>'+source.url+'</td><td>'+source.ext_collected_amount+'</td><td>'+source.ext_initial_target+'</td><td>'+source.ext_nb_contributors+'</td><td>'+source.ext_end_date+'</td></tr>')
				}else{
					alert("Ce source existe déjà")
				}
			}else{
				alert("Erreur! Merci de vérifier les données saisies");
				return false;
			}
		})
	})



	//------------------------
	//edit scraping source
	//------------------------

	$(".edit_source").off("click").on("click",function(){
		var end_date = $(this).parent().prev();
		var nb_contributors = end_date.prev();
		var initial_target = nb_contributors.prev();
		var collected_amount = initial_target.prev();
		var url = collected_amount.prev();
		var name = url.prev(); 
		$("#edit_scraping_source_name").val(name.text());
		$("#edit_scraping_source_url").val(url.text());
		$("#edit_scraping_source_exp_ca").val(collected_amount.text());
		$("#edit_scraping_source_exp_it").val(initial_target.text());
		$("#edit_scraping_source_exp_nc").val(nb_contributors.text());
		$("#edit_scraping_source_exp_ed").val(end_date.text());
		$("#edit_scraping_source_id").val($(this).attr('ss-id'))
	})

	$("#submit_edit_scraping_source").off("click").on("click", function(){
		$("#edit_scraping_source").submit();

		$("#edit_scraping_source").on("ajax:success", function(evt, result, status){	
			if (result.success){
				console.log(result.scraping_source);
				var name = $("tr[ss-id="+result.scraping_source.id+"] td:first-child");
				var url = name.next();
				var collected_amount = url.next();
				var initial_target = collected_amount.next();
				var nb_contributors = initial_target.next();
				var end_date = nb_contributors.next();

				name.text(result.scraping_source.name);
				url.text(result.scraping_source.url);
				collected_amount.text(result.scraping_source.ext_collected_amount);
				initial_target.text(result.scraping_source.ext_initial_target);
				nb_contributors.text(result.scraping_source.ext_nb_contributors);
				end_date.html(result.scraping_source.ext_end_date);

			}else{
				alert("Erreur! Merci de vérifier les données saisies");
				return false;
			}
		})

	})

	// Delete source 

	$(".delete-source").on("ajax:success", function(evt, result, status){
		if( result.success ){
			$(this).parent().parent().remove();
		}
	})

	// Submit check import
	$("#submit_check_import_form").off('click').on("click", function(){
		$("#check_import_form").submit();
		$(this).prop('disabled', true);
		$(this).text("Importation des données ....")

		$("#check_import_form").on("ajax:success", function(evt, result, status){
			console.log(result);
			$("#submit_check_import_form").prop('disabled', false);
			$("#submit_check_import_form").text("Vérifier l'import des données");
		})
	})


	
})