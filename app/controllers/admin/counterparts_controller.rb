class Admin::CounterpartsController < AdminController

	def index
		@counterparts = Counterpart.page(params[:page]).per_page(10) 
	end

end