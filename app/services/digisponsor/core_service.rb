class Digisponsor::CoreService
	include HTTParty
	URI_API = "https://test.digisponsor.fr"
	base_uri "#{URI_API}/api/service/restv1/"
	OPTIONS = {body: { email: "myadmin@thrinacia.com", password: "OcD0ebVnf9" }}


	class << self

		def base_uri_image
			"#{URI_API}/api/image/campaign_profile/"
		end

		def authenticate
			response  = post('/authenticate', OPTIONS)
			# Log authentication problem
			default_cookies.add_cookies(response.headers["set-cookie"])
			# if authenticated successfully
			{success: true}
		end

		def migration_status
			@@migration_status ||= :init
		end

		def migration_started?
			migration_status == :started 
		end

		def migration_started
			@@migration_status = :started
		end

		def migration_finished
			logger.info "END Migration"
			@@migration_status = :finished
		end

		def migration_init
			@@migration_status = :init
		end

	end



end