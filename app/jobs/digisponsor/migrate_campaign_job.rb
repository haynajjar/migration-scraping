class Digisponsor::MigrateCampaignJob < ApplicationJob
  

  def perform 
    Digisponsor::CampaignService.migrate 
  end

end