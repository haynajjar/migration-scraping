class Digisponsor::MigrateCounterpartJob < ApplicationJob
  

  def perform 
    Digisponsor::CounterpartService.migrate 
  end

end