class CreateScrapingOrigins < ActiveRecord::Migration[5.0]
  def change
    create_table :scraping_origins do |t|
      t.string :link
      t.integer :frequency
      t.integer :scraping_source_id

      t.timestamps
    end
  end
end
