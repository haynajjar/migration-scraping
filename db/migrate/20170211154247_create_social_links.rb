class CreateSocialLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :social_links do |t|
      t.string :social_link_id
      t.string :uri_text
      t.string :uri
      t.integer :user_id
      t.timestamps
    end
  end
end
