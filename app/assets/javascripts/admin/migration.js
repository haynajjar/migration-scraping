function start_user_migration_interval(url){
	var migration_link = $("#start_user_migration");
	migration_link.addClass("disabled");
	migration_link.text("En cours de migration ...");
	var interval_migration = setInterval(function() {
			$.ajax({
				url: url,
				dataType: "json",
				success: function(res){
					if(res.migration_finished){
						clearInterval(interval_migration);
						migration_link.removeClass("disabled");
						migration_link.text("Lancer la migration")
					}
					$("#migration_user_log").html(res.log);
				}
			})
		}, 3000);
}

function start_campaign_migration_interval(url){
	var migration_link = $("#start_campaign_migration");
	migration_link.addClass("disabled");
	migration_link.text("En cours de migration ...");
	var interval_migration = setInterval(function() {
			$.ajax({
				url: url,
				dataType: "json",
				success: function(res){
					if(res.migration_finished){
						clearInterval(interval_migration);
						migration_link.removeClass("disabled");
						migration_link.text("Lancer la migration")
					}
					$("#migration_campaign_log").html(res.log);
				}
			})
		}, 3000);
}

function start_counterpart_migration_interval(url){
	var migration_link = $("#start_counterpart_migration");
	migration_link.addClass("disabled");
	migration_link.text("En cours de migration ...");
	var interval_migration = setInterval(function() {
			$.ajax({
				url: url,
				dataType: "json",
				success: function(res){
					if(res.migration_finished){
						clearInterval(interval_migration);
						migration_link.removeClass("disabled");
						migration_link.text("Lancer la migration")
					}
					$("#migration_counterpart_log").html(res.log);
				}
			})
		}, 3000);
}


document.addEventListener("turbolinks:load", function() {


	$("#start_user_migration").on("ajax:success", function(evt, result, status){
		if(result.success){
			start_user_migration_interval(result.redirect)
		}else{
			alert(result.message)
		}
	})
	$("#start_campaign_migration").on("ajax:success", function(evt, result, status){
		if(result.success){
			start_campaign_migration_interval(result.redirect)
		}else{
			alert(result.message)
		}
	})
	$("#start_counterpart_migration").on("ajax:success", function(evt, result, status){
		if(result.success){
			start_counterpart_migration_interval(result.redirect)
		}else{
			alert(result.message)
		}
	})
})