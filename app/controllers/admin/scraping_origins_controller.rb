class Admin::ScrapingOriginsController < AdminController

	before_action :set_origins

	def create
		unless @scraping_origin = ScrapingOrigin.find_by(link: scraping_origin_params[:link])
			@scraping_origin = ScrapingOrigin.create(scraping_origin_params.merge(scraping_source_id: params[:scraping_source_id]))
		end
		# perform job 
		ScrapingJob.perform_later @scraping_origin
		flash[:notice] = "Votre lien à été associé avec succès"
		render :show
	end

	def show
		@scraping_origin = ScrapingOrigin.find(params[:id])
		@scraping_data = @scraping_origin.scraping_data.recent.page(params[:page]).per_page(10);
	end

	private
	def set_origins
		@scraping_origins = ScrapingOrigin.all
	end
	def scraping_origin_params
		params.require(:scraping_origin).permit(:frequency, :link, :end_date)
	end

end