class AdminController < ApplicationController
	# user should have administration role
	# since this is not added in the specification document 
	# we're not going to add it

	layout 'admin'

	def index
		redirect_to admin_scraping_sources_path
	end
end