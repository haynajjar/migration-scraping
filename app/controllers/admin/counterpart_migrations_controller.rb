class Admin::CounterpartMigrationsController < AdminController

	def index
		@migration_log = File.read("#{Rails.root}/log/digisponsor_counterparts_migration.log").gsub("\n","<br>").html_safe rescue "No Log file"
		@migration_started = Digisponsor::CounterpartService.migration_started?
	end

	def start
		# start migration job
		if Digisponsor::CounterpartService.migration_started?
			render json: {success: false, message: "Migration Already Started"}
		else
			Digisponsor::CounterpartService.migration_init
			Digisponsor::MigrateCounterpartJob.perform_later
			render json: {success: true, redirect: show_log_admin_counterpart_migrations_path}
		end
	end

	def show_log
		# show log every second
		@migration_log = File.read("#{Rails.root}/log/digisponsor_counterparts_migration.log").gsub("\n","<br>").html_safe 
		render json: {log: @migration_log.gsub("\n","<br>").html_safe, migration_finished: !Digisponsor::CounterpartService.migration_started?}
	end

end