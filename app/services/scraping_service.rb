class ScrapingService

	class << self

		def check_import url
			if url.scan(URI.regexp)
				uri = URI(url)
				# get hosts from scraping sources
				hosts = ScrapingSource.pluck(:id,:url).map{|ss| [ss[0], URI(ss[1]).host] }
				selected_host = hosts.detect {|e| e[1] == uri.host}
				if  selected_host.present?
					#selected_host[0] returns the id of the founded host
					check_data_scraping url, selected_host[0]
				else 
					{success: false, reason: :host_not_found}
				end
			else
				{success: false, reason: :not_url}
			end
		end

		# used to update frequency , set it to 0 in case date is not found or the project is expired
		# the frequency set used to prevent the scraping jop
		def update_frequency scraping_origin , end_date

			if Date.today > end_date
				scraping_origin.update(frequency: 0)
			end 
		end

		private
		def check_data_scraping url, scraping_source_id
			scraping_source = ScrapingSource.find(scraping_source_id)
			page = Nokogiri::HTML(open(url)) 
			collected_amount = page.css(scraping_source.ext_collected_amount).first.text rescue nil
			initial_target = page.css(scraping_source.ext_initial_target).first.text rescue nil
			nb_contributors = page.css(scraping_source.ext_nb_contributors).first.text rescue nil
			end_date = page.css(scraping_source.ext_end_date).first.text rescue nil

			if valid(collected_amount, initial_target, nb_contributors, end_date)
				{success: true, scraping_data: {collected_amount: collected_amount, 
														initial_target: initial_target, 
														nb_contributors: nb_contributors, 
														end_date: end_date, 
														scraping_source_id: scraping_source_id,
														origin_url: url
														}}
			else
				{success: false, reason: :not_valid}
			end
		end

		# check if any of the values is collected
		def valid collected_amount, initial_target, nb_contributors, end_date
			# string.nan? is implemented in initializers/monkey_patches.rb  
			(!collected_amount.nan? rescue false) || initial_target.present? || (!nb_contributors.nan? rescue false) || (Date.parse(end_date).present? rescue false)
		end



	end
end