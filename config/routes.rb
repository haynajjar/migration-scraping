Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'authentication#login'

  get 'authentication/login', as: 'login'
  post 'authentication/sign_in', as: 'sign_in'

  resources :admin, only: :index

  namespace :admin do
  	 resources :scraping_sources, only: [:index, :destroy] do
  	 	collection do  
  	 		post 'add_source'
  	 		post 'edit_source'
  	 		post 'check_import'
  	 	end
  	 	resources :scraping_origins, only: :create
  	 end
  	 resources :scraping_origins, only: :show
     resources :user_migrations, only: :index do 
      collection do 
        get 'start'
        get 'show_log'
      end
     end     
     resources :campaign_migrations, only: :index do 
      collection do 
        get 'start'
        get 'show_log'
      end
     end     
     resources :counterpart_migrations, only: :index do 
      collection do 
        get 'start'
        get 'show_log'
      end
     end

     resources :users, only: :index
     resources :campaigns, only: :index
     resources :counterparts, only: :index
  end
end
