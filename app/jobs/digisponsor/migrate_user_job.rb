class Digisponsor::MigrateUserJob < ApplicationJob
  

  def perform 
    Digisponsor::UserService.migrate 
  end

end