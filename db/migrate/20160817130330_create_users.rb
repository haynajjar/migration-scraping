class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :l_name
      t.string :email
      t.string :ville
      t.string :lign1
      t.string :lign2
      t.string :pays
      t.string :state
      t.string :cp
      t.string :tel1
      t.string :description
      t.string :interests     , array: true
      t.string :birth_date
      t.string :nationality   , array: true
      t.string :langue
      t.string :link_facebook
      t.string :link_linkedin
      t.string :link_other
      t.string :photo
      t.string :salt
      t.string :password_hash
      t.string :language
      t.string :id_customer
      t.string :followed_campaigns   , array: true
      t.string :time_zone
      t.string :currency
      t.string :role
      t.string :token
      t.datetime :expires_at
      t.string :password_rest_link
      t.string :confirm_mail_link
      
    end
    
    User.create(:role => "administrator", :name => "admin", :email => "admin@admin.com", :salt => "ae62e8b646bf89f4c87bdba1a61ad98f", :password_hash => "78efc90d8049d6c7b8144f917955e7c2e41d3e0a3056e20503ef1d081ce9a653")
  end
end
