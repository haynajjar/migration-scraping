class CreateBusinessOrganizations < ActiveRecord::Migration[5.0]
  def change
    create_table :business_organizations do |t|
      t.string :organization_id
      t.string :name
      t.text :description
      t.string :links, array: true
      t.string :links_texts, array: true
      t.string :image_name
      t.string :image_path
      t.integer :user_id

      t.timestamps
    end
  end
end
