class AddMigrationIdToCounterparts < ActiveRecord::Migration[5.0]
  def change
    add_column :counterparts, :migration_id, :string
  end
end
