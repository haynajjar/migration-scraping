class Digisponsor::UserService < Digisponsor::CoreService

	
	class << self
		

		def migrate
			# get list users ordered by created desc
			# we assume that the list is ordered (it seems like that..)
			# CHECK if migration already started
			unless migration_started?
				migration_started
				begin
					init_logger
					logger.info "Call API users ..."
					response = call_users
					if response[:success]
						start_migration response[:users]
					else
						# can't find users
						logger.error "Can't call user API , #{response[:message]}"
					end
					migration_finished
				rescue StandardError => e 
					migration_finished
					logger.error e.message
					p e.backtrace
				end
			else
				{success: false, message: "Migration already started"}
			end
		end


		def call_users
			if cookies[:expires].present? && cookies[:expires].to_datetime > DateTime.now
				{success: true, users: get_users}
			else
				# LOG inti cookies
				logger.info "Authenticate ...."
				if authenticate
					response = get_users
					# LOG users issue
					{success: true, users: response}
				else
					# LOG authentication problem in users call
					{success: false, message: "can't authenticate"}
				end
			end
		end

		def start_migration users
			# LOG Start users migration ..
			logger.info "Start migration ..."
			users.each do |user|
				logger.info "User #{user['id']} #{user['email']}"
				logger.info "Check existing user ..."
				if User.find_by(email: user["email"]).nil?
					logger.info "#{user['email']} is a new user"
					migrate_user user, :create 
				else
					logger.info "User #{user['email']} exists" 
					migrate_user user, :update 
				end
			end
		end


		def migrate_user user , action=:create
			# LOG migrate user X
			# migrate profile
			# migrate address
			logger.info "Migrate user #{user['email']} ..." 
			user_id = user["id"]
			profile = get_profile user_id
			user_constructor({name: profile["first_name"], l_name: profile["last_name"], email: user['email'], description: profile["bio"]}, :init)
			addresses = get_addresses user_id
			addrs = addresses["personal"].present? ? addresses["personal"] : addresses["business"]
			if addrs.present?
				logger.info "* User has addresses *"
				logger.info "* Add addresses *"
				addr = addrs.first
				user_constructor({ville: addr["city"], pays: addr["country"], lign1: addr["street1"], lign2: addr["street2"], cp: addr["mail_code"]})
			end
			phone_numbers = get_phone_numbers(user_id)
			app_user = user_constructor({tel1: extract_phone_numbers(phone_numbers)}, action, user)
			# migrate business organization
			business_organizations = get_business_organizations user_id
			migrate_business business_organizations, user_id, app_user 
			
			#migrate social links
			social_links = get_social_links(user_id)
			migrate_social_links social_links, user_id, app_user
			# save social links
		end

		def migrate_business business_organizations, user_id, app_user
			logger.info "$ Migrate business organizations ..."
			business_organizations.each do |org|
				org_id = org["id"]
				# we need to add ['business'] -- business link  
				org_links = get_business_organization_websites(user_id, org_id)["business"]
				org_files = get_business_organization_files user_id, org_id
				# save business organization
				b_organization_hash = extract_business_organization org, org_links, org_files 
				if app_user.business_organizations.any? && borganization = BusinessOrganization.find_by(organization_id: org_id)
					logger.info "Organization already exist"
					logger.info "Updating organization #{b_organization_hash['name']} ..."
					# update business organization
					borganization.update(b_organization_hash)
				else
					# create business organization
					logger.info "Adding new business organization #{b_organization_hash['name']} ..."
					app_user.business_organizations << BusinessOrganization.create(b_organization_hash)
				end
			end
			logger.info "$ End organizations migration "
		end

		def migrate_social_links social_links, user_id, app_user
			logger.info "$ Migrate social links ..."
			social_links.each do |slink|
				slink_id = slink["id"]
				uri_text = slink["uri_text"]
				uri = slink["uri"]
				slink_hash = {social_link_id: slink_id, uri_text: uri_text, uri: uri}
				# if social link already exists
				if app_user.social_links.any? && cuser_link = app_user.social_links.find_by(social_link_id: slink_id)
					# update social link 
					logger.info "Social link already exist"
					logger.info "Updating social link #{slink_hash['uri_text']} ..."
					cuser_link.update(slink_hash)
				else
					logger.info "Adding new social link #{slink_hash['uri_text']}"
					# create new social link
					app_user.social_links << SocialLink.create(slink_hash)
				end
			end
		end


		#extract and save BusinessOrganization
		def extract_business_organization org, org_links, org_files
			# extract first image
			image_name = org_files.first["name"]
			image_path = base_uri_image + org_files.first["path_external"]
			# extract links
			links = org_links.map{|link| link["uri"]}
			links_texts = org_links.map{|link| link["uri_text"]}

			org_name = org["name"]
			description = org["description"]

			{organization_id: org["id"] ,name: org_name, description: description, links: links, links_texts: links_texts, image_name: image_name,image_path: image_path}
		end


		def extract_phone_numbers phone_numbers
			perso_numbers = phone_numbers["personal"].present? ? phone_numbers["personal"].map{|n| n["number"]} : []
			business_numbers = phone_numbers["business"].present? ? phone_numbers["business"].map{|n| n["number"]} : []			
			logger.info "# Extract phone numbers , #{perso_numbers.count} personal and #{business_numbers.count} business"
			(perso_numbers + business_numbers).join(", ")
		end


		def get_profile user_id
			logger.info "Getting profile for user #{user_id} ..."
			get("/account/person/#{user_id}")
		end


		def get_phone_numbers user_id
			logger.info "Getting phone numbers for user #{user_id} ..."
			get("/account/phone-number/?person_id=#{user_id}")
		end

		def get_addresses user_id
			logger.info "Getting addresses for user #{user_id} ..."
			get("/account/address/?person_id=#{user_id}")
		end


		def get_business_organizations user_id
			logger.info "Getting business organizations for user #{user_id}"
			get("/account/business/?person_id=#{user_id}")
		end

		def get_business_organization_websites user_id, org_id 
			logger.info "Getting DETAILS for business organization #{org_id} for user #{user_id} ..."
			get("/account/website/?person_id=#{user_id}&business_organization_id=#{org_id}")
		end

		def get_business_organization_files user_id, org_id 
			logger.info "Getting IMAGE for business organization #{org_id} for user #{user_id} ..."
			get("/account/resource/file?person_id=#{user_id}&business_organization_id=#{org_id}")
		end

		def get_social_links user_id
			logger.info "Getting social links for user #{user_id}"
			get("/account/website/?person_id=#{user_id}")["personal"]
		end

		def get_users
			logger.info "Getting users list ..."
			get('/portal/person')
		end



		def user_constructor attrs, status=:merge ,user=nil
			if status == :init 
				@@current_user = {}
			end
			@@current_user.merge!(attrs)
			if status == :create 
				User.create(@@current_user)
			elsif status == :update
				user = User.find_by(email: user['email'])
				user.update(@@current_user)
				user	
			end
		end


		def logger
			@@user_logger 
		end

		def init_logger
			begin
				File.delete("#{Rails.root}/log/digisponsor_user_migration.log")
			rescue StandardError => e 
				puts "Logger File Not found, creating new one ..."
			ensure
				@@user_logger = Logger.new("#{Rails.root}/log/digisponsor_user_migration.log")
			end
		end

	


	end
end