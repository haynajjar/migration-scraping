class Admin::UserMigrationsController < AdminController

	def index
		@migration_log = File.read("#{Rails.root}/log/digisponsor_user_migration.log").gsub("\n","<br>").html_safe rescue "No log file"
		@migration_started = Digisponsor::UserService.migration_started?
	end

	def start
		# start migration job
		if Digisponsor::UserService.migration_started?
			render json: {success: false, message: "Migration Already Started"}
		else
			Digisponsor::UserService.migration_init
			Digisponsor::MigrateUserJob.perform_later
			render json: {success: true, redirect: show_log_admin_user_migrations_path}
		end
	end

	def show_log
		# show log every second
		@migration_log = File.read("#{Rails.root}/log/digisponsor_user_migration.log").gsub("\n","<br>").html_safe
		render json: {log: @migration_log.gsub("\n","<br>").html_safe, migration_finished: !Digisponsor::UserService.migration_started?}
	end

end