# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170212130735) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "business_organizations", force: :cascade do |t|
    t.string   "organization_id"
    t.string   "name"
    t.text     "description"
    t.string   "links",                        array: true
    t.string   "links_texts",                  array: true
    t.string   "image_name"
    t.string   "image_path"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "campaign_campain_categories", id: false, force: :cascade do |t|
    t.integer "campaing_id"
    t.integer "campaing_category_id"
  end

  create_table "campaign_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "campaigns", force: :cascade do |t|
    t.string   "url"
    t.string   "type_campaign"
    t.string   "short_url"
    t.string   "name"
    t.string   "headline"
    t.string   "funding_status"
    t.string   "campaign_status"
    t.integer  "current_funding"
    t.string   "id_stripe"
    t.string   "stripe_payment_token"
    t.string   "essential_description_title"
    t.string   "essential_description_presentation"
    t.string   "essential_description_image"
    t.string   "essential_description_type"
    t.string   "essential_description_subtype"
    t.string   "essential_objective_type"
    t.integer  "essential_objective_goal_funding"
    t.integer  "essential_objective_minimum_funding"
    t.string   "essential_objective_currency"
    t.integer  "essential_objective_goal_commands"
    t.integer  "essential_objective_minimum_commands"
    t.integer  "essential_objective_count_commands"
    t.integer  "essential_objective_goal_votes"
    t.integer  "essential_objective_count_votes"
    t.integer  "essential_objective_minimum_votes"
    t.string   "pc_f_name"
    t.string   "pc_l_name"
    t.datetime "pc_birth_date"
    t.string   "pc_phone"
    t.string   "pc_country"
    t.string   "pc_street"
    t.string   "pc_city"
    t.integer  "pc_postal_code"
    t.datetime "campaign_start_date"
    t.integer  "campaign_duration"
    t.datetime "campaign_limit_date"
    t.string   "pitch_video_url"
    t.string   "pitch_category"
    t.string   "pitch_description"
    t.string   "pitch_page_adress"
    t.integer  "pitch_audience_number"
    t.boolean  "pitch_is_event"
    t.boolean  "stripe_available"
    t.string   "pitch_place_name"
    t.string   "pitch_place_adress"
    t.string   "about"
    t.string   "about_html"
    t.string   "approved_at"
    t.datetime "created_at"
    t.string   "notes"
    t.datetime "updated_at"
    t.string   "facebook_link"
    t.string   "messenger_link"
    t.string   "twitter_link"
    t.datetime "repayment_deadline"
    t.string   "pitch_tags",                           array: true
    t.integer  "rise_mode"
    t.string   "campaign_classification"
    t.string   "forther_information"
    t.string   "chaeckout_message"
    t.string   "twitter_message"
    t.string   "facebook_message"
    t.string   "headline_text_above"
    t.string   "subheadline_text_above"
    t.string   "text_below_form"
    t.string   "service_tearm_url"
    t.string   "privancy_policy_url"
    t.datetime "pitch_fin_evenement"
    t.datetime "picht_debut_evenemet"
    t.string   "migration_id"
    t.string   "categories",                           array: true
  end

  create_table "counterparts", force: :cascade do |t|
    t.integer  "amount"
    t.string   "title"
    t.string   "description"
    t.datetime "shipping_date"
    t.string   "name"
    t.integer  "max_number"
    t.integer  "current_number"
    t.boolean  "to_deliver"
    t.string   "counterpart_type"
    t.string   "notes"
    t.integer  "campaign_id"
    t.string   "image"
    t.string   "type"
    t.integer  "barred_price"
    t.boolean  "sisplay_barred_price"
    t.integer  "deposit"
    t.boolean  "cash_deposit"
    t.boolean  "highlight"
    t.boolean  "display_variant"
    t.integer  "shipper_rule_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "migration_id"
  end

  create_table "scraping_data", force: :cascade do |t|
    t.string   "collected_amount"
    t.string   "initial_target"
    t.string   "nb_contributors"
    t.string   "end_date"
    t.integer  "scraping_origin_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.float    "initial_target_f"
    t.float    "collected_amount_f"
  end

  create_table "scraping_origins", force: :cascade do |t|
    t.string   "link"
    t.integer  "frequency"
    t.integer  "scraping_source_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.date     "end_date"
  end

  create_table "scraping_sources", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.string   "ext_collected_amount"
    t.string   "ext_initial_target"
    t.string   "ext_nb_contributors"
    t.string   "ext_end_date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "social_links", force: :cascade do |t|
    t.string   "social_link_id"
    t.string   "uri_text"
    t.string   "uri"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "l_name"
    t.string   "email"
    t.string   "ville"
    t.string   "lign1"
    t.string   "lign2"
    t.string   "pays"
    t.string   "state"
    t.string   "cp"
    t.string   "tel1"
    t.string   "description"
    t.string   "interests",          array: true
    t.string   "birth_date"
    t.string   "nationality",        array: true
    t.string   "langue"
    t.string   "link_facebook"
    t.string   "link_linkedin"
    t.string   "link_other"
    t.string   "photo"
    t.string   "salt"
    t.string   "password_hash"
    t.string   "language"
    t.string   "id_customer"
    t.string   "followed_campaigns", array: true
    t.string   "time_zone"
    t.string   "currency"
    t.string   "role"
    t.string   "token"
    t.datetime "expires_at"
    t.string   "password_rest_link"
    t.string   "confirm_mail_link"
  end

end
