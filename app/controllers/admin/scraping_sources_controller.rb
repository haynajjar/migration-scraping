class Admin::ScrapingSourcesController < AdminController

	def index 
		#used in modal to show the table
		@scraping_sources = ScrapingSource.all
	end

	def add_source
		# add for different origin
		if @scraping_source = ScrapingSource.find_by(url: scraping_source_params[:url])
			render json: {success: true, exist: true, scraping_source: @scraping_source}		
		else
			@scraping_source = ScrapingSource.create(scraping_source_params)
			render json: {success: true, scraping_source: @scraping_source}		
		end
	end

	def edit_source
		@scraping_source = ScrapingSource.find(scraping_source_params[:id])
		if @scraping_source.update(scraping_source_params.except(:id))
			render json: {success: true, scraping_source: @scraping_source}
		else
			render json: {success: false}
		end
	end

	def check_import
		url = params[:scraping_url]
		@scraping_result = ScrapingService.check_import(url)
		if @scraping_result[:success]
			@scraping_source = ScrapingSource.find(@scraping_result[:scraping_data][:scraping_source_id])
			@scraping_origin = ScrapingOrigin.new
		end
	end	


	def destroy
		scraping_source = ScrapingSource.find(params[:id])
		scraping_source.destroy
		render json: {success: true}
	end

	private
	def scraping_source_params
		params.require(:scraping_source).permit!
	end

end