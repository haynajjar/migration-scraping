class AddAttrsNumToScrapingData < ActiveRecord::Migration[5.0]
  def change
    add_column :scraping_data, :initial_target_f, :float
    add_column :scraping_data, :collected_amount_f, :float
  end
end
